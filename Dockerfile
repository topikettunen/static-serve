FROM golang:latest

WORKDIR /go/src/github.com/topikettunen/app/
COPY main.go main.go

RUN CGO_ENABLED=0 GOOS=linux go install .

ENTRYPOINT [ "app" ]
